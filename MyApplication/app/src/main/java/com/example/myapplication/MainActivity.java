package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Vi_Vind> lstVi;
    ViAdapter viAdapter;
    LinearLayout layoutEdit;
    Intent intent;
    int select = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControls();
        addDataDemo();
//        intent = new Intent(MainActivity.this, Detail.class);
    }

    private void addDataDemo() {
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));
        lstVi.add(new Vi_Vind(R.drawable.nguhanhson,  "Chuỗi siêu thị uy tín. Sản phẩm đa dạng, vận chuyện siêu tốc", "VinMart", "5%"));

        viAdapter = new ViAdapter(R.layout.vi_activity, lstVi, this);
        listView.setAdapter(viAdapter);
    }

    private void addControls() {
        lstVi = new ArrayList<Vi_Vind>();
        listView = findViewById(R.id.listview);
    }
}