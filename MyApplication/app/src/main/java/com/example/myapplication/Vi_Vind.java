package com.example.myapplication;

public class Vi_Vind {
    private int img;
    private String title;
    private String gioi_thieu;
    private String tich_diem;

    public Vi_Vind(int img, String title, String gioi_thieu, String tich_diem) {
        this.img = img;
        this.title = title;
        this.gioi_thieu = gioi_thieu;
        this.tich_diem = tich_diem;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGioi_thieu() {
        return gioi_thieu;
    }

    public void setGioi_thieu(String gioi_thieu) {
        this.gioi_thieu = gioi_thieu;
    }

    public String getTich_diem() {
        return tich_diem;
    }

    public void setTich_diem(String tich_diem) {
        this.tich_diem = tich_diem;
    }
}
