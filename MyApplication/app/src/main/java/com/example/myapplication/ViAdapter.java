package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ViAdapter extends BaseAdapter {
    int layout;
    ArrayList<Vi_Vind> mArrayList;
    Context mContext;

    public ViAdapter(int layout, ArrayList<Vi_Vind> mArrayList, Context mContext) {
        this.layout = layout;
        this.mArrayList = mArrayList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(layout, null);
        Vi_Vind vi_vind = (Vi_Vind) getItem(position);
        ImageView img = convertView.findViewById(R.id.img_vi);
        TextView title = convertView.findViewById(R.id.gioi_thieu);
        TextView gioi_thieu = convertView.findViewById(R.id.titleTv);
        TextView tich = convertView.findViewById(R.id.tich_diem);
        img.setImageResource(vi_vind.getImg());
        title.setText(vi_vind.getTitle());
        gioi_thieu.setText(vi_vind.getGioi_thieu());
        tich.setText(vi_vind.getTich_diem());
        return convertView;
    }
}
